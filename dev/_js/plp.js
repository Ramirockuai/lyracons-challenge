$(function(){
	// Toggle filters
	$('.filter-accordion__title').on('click', function(e){
		e.preventDefault();
		$(this).closest('.filter-accordion').toggleClass('active');
	});

	// Show filters
	$('.plp-filters__trigger').on('click', function(e){
		e.preventDefault();
		$('body').addClass('filters-active');
	});

	// Hide filters
	$('.plp-filters__close, .plp-filters__apply').on('click', function(e){
		e.preventDefault();
		$('body').removeClass('filters-active');
	});

	// Activate Filters
	$('.filter-accordion__item-link').on('click', function(e){
		e.preventDefault();
		$(this).toggleClass('active');
	});

	// Sort
	$('#show-sort-options').on('click', function(e){
		e.preventDefault();
		$('.plp-sort__content').toggleClass('active');
	});
	$('.plp-sort__item-link').on('click', function(e){
		e.preventDefault();
		let current_sort_order = $(this).text();
		$('.plp-sort__content').removeClass('active');
		$('#plp-sort__item-active').html(current_sort_order);
	})

	// Load more products
	$('#load-more-products').on('click', function(e){
		e.preventDefault();
		let that = $(this);
		$.ajax({
			url: 'js/products.json',
			success: function(data){
				console.log(data);
				let new_products='';
				$.each(data.products, function(index, item){
					let listPrice = parseFloat( item.listPrice ),
						bestPrice = parseFloat( item.bestPrice );;
					new_products += `
						<li class="product-item__wrapper" data-id="${item.id}" id="product-item__wrapper-${item.id}">
							<div class="product-item">
								<a href="#" class="product-item__image">
									<picture>
										<source media="(max-width: 767px)" srcset="${item.imageSrcMobile}">
										<img src="${item.imageSrc}" width="289" height="419" alt="${item.name}">
									</picture>
								</a>
								<h3 class="product-item__name">${item.name}</h3>
								<div class="product-item__price promo">
									<div class="product-item__list-price">$${listPrice.formatMoney(2, ',', '.')}</div>
									<div class="product-item__sell-price">$${bestPrice.formatMoney(2, ',', '.')}</div>
								</div>
								<div class="product-item__installments">Hasta <strong>${item.fees} cuotas</strong></div>
							</div>
						</li>
					`;
				});
				$('#plp-items__list').append(new_products);
				that.slideUp(500, function(){
					$(this).remove();
				});
			}
		});
	});
});