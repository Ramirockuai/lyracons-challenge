//number of decimals, decimal, separator
Number.prototype.formatMoney = function(c, d, t){
var n = this,
	c = isNaN(c = Math.abs(c)) ? 2 : c,
	d = d == undefined ? "." : d,
	t = t == undefined ? "," : t,
	s = n < 0 ? "-" : "",
	i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
	j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
$(function(){
	// Prevent empty links click
	$('body').on('click','a[href=""], a[href="#"]',function(e){
		var that=$(this),
			uri=that.attr('href');
		if(uri=='' || uri=='#'){
			e.preventDefault();
			return false;
		}
	});

	// Mobile Menu
	$('.mainnav__trigger').on('click', function(e){
		e.preventDefault();
		$('body,html').toggleClass('mainnav-visible');
	});
	$('.overlay').on('click', function(e){
		e.preventDefault();
		$('body,html').toggleClass('mainnav-visible');
	});
	// Submenu
	$('.submenu__trigger').on('click', function(e){
		e.preventDefault();
		$(this).closest('.submenu__wrapper').toggleClass('submenu__active');
	});

	// Hide / show product items
	$('a.cta-login').on('click', function(e){
		e.preventDefault();
		$('#plp-items__list').stop(true, true).slideToggle(500);
	});

	// Change title color
	$('.mainnav__item.winter a').on('mouseenter', function(){
		$('h1.plp__title').addClass('active');
	});
	$('.mainnav__item.winter a').on('mouseleave', function(){
		$('h1.plp__title').removeClass('active');
	});
});