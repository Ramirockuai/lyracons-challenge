<!--
<svg class="svg-icon item"><use xlink:href="#svg-icon-item"></use></svg>
-->
<header class="site-header">
	<div class="cintillo">
		<div class="container">
			<div class="cintillo__text"><strong>¡Aprovechá la promo!</strong><span>Comprá hasta 12 cuotas sin interés</span></div>
		</div>
	</div>
	<div class="mainnav__trigger">
		<span class="close"></span>
	</div>
	<div class="overlay"></div>
	<div class="mainnav__wrapper">
		<div class="container">
			<div class="site-brand"><a href="/"><svg class="site-brand__svg"><use xlink:href="#svg-icon-logo"></use></svg></a></div>
			<nav class="mainnav">
				<ul class="mainnav__list site">
<?php foreach($mainnav as $nav){ ?>
	<li class="mainnav__item <?=$nav['class']?>">
		<a href="<?=$nav['link']?>" target="<?=$nav['target']?>"><?=$nav['html']?></a>
		<?php if($nav['subnav']){ ?>
			<div class="submenu__trigger"><svg class="svg-icon arrow-right"><use xlink:href="#svg-icon-arrow-right"></use></svg></div>
			<ul class="submenu">
				<?php foreach($nav['nav'] as $subnav){ ?>
					<li class="submenu__item"><a href="<?=$subnav['link']?>" target="<?=$subnav['target']?>"><?=$subnav['html']?></a></li>
				<?php } ?>
			</ul>
		<?php } ?>
	</li>
<?php } ?>
					<li class="mainnav__item-contact first last hidden-lg-up">
						<a href="#" class="cta-login">Ingresar</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	<div class="searchbar__wrapper">
		<div class="searchbar">
			<input type="text" placeholder="BUSCAR" class="searchbar__input">
			<button class="searchbar__button"><svg><use xlink:href="#svg-icon-search"></use></svg></button>
		</div>
	</div>
	<div class="second-nav">
		<ul class="second-nav__list">
			<li class="second-nav__item account">
				<a href="#" class="cta-login">Ingresar</a>
			</li>
			<li class="second-nav__item minicart">
				<a class="minicart__trigger" id="minicart__trigger" href="#">
					<span>Carrito</span>
					<div class="minicart__trigger-icon">
						<svg class="cart"><use xlink:href="#svg-icon-cart"></use></svg>
						<div id="total-items">3</div>
					</div>
				</a>
			</li>
		</ul>
	</div>
</header>