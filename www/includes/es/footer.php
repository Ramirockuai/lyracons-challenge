<footer class="site-footer footer">
	<div class="container">
		<div class="foonav">
			<ul class="foonav__list">
				<li class="foonav__item"><a href="#">CAMBIOS Y DEVOLUCIONES</a></li>
				<li class="foonav__item"><a href="#">ENVÍOS</a></li>
				<li class="foonav__item"><a href="#">MEDIOS DE PAGO</a></li>
				<li class="foonav__item"><a href="#">PREGUNTAS FRECUENTES</a></li>
				<li class="foonav__item"><a href="#">VENTA MAYORISTA</a></li>
			</ul>
		</div>
		<div class="newsletter__wrapper">
			<h2 class="newsletter__title">News</h2>
			<p class="newsletter__desc">Suscribite y entérate de las promos!</p>
			<input type="text" class="newsletter__input" placeholder="Escribí tu Mail">
			<button class="btn-flat primary upp">SUSCRIBITE</button>
		</div>
	</div>
</footer>