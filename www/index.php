<?php
$abs_path = $_SERVER['DOCUMENT_ROOT'];
include("$abs_path/includes/config.php");
include("$abs_path/includes/products.php");
$page_class="home loading";
$page_title="$brand_name | Online Store";
$page_desc="";
$page_keywords="";
$page_url="$http_s://$domain";
$page_img="https://www.lyracons.com/app/uploads/2019/08/herobanner_eCommerce-2.jpg";
$lang="es";
$locale="es_ES";
setlocale(LC_MONETARY, $locale);
?>
<!DOCTYPE html>
<html lang="<?=$lang?>">
<head>
<?php include("$abs_path/includes/head.php");?>
</head>
<body class="<?=$page_class?>">
<?php include("$abs_path/includes/tags-init-body.php");?>
<!-- Header -->
<?php include("$abs_path/includes/$lang/header.php");?>
<!-- / End Header -->

<main class="main-plp">
	<div class="container">
		<div class="breadcrumb">
			<ul>
				<li>
					<a href="#">Invierno 2020</a>
				</li>
			</ul>
		</div>
		<h1 class="plp__title">Título de categoría</h1>
	</div>
	<div class="container">
		<div class="plp__content">
			<aside class="plp-filters__wrapper">
				<h3 class="plp-filters__title">
					Filtrar por<span class="hidden-lg-up">:</span>
				</h3>
				<a href="#" class="plp-filters__close hidden-lg-up" id="plp-filters__close">
					<svg class="close"><use xlink:href="#svg-icon-close"></use></svg>
				</a>
				<div class="filter-accordion__wrapper">
					<div class="filter-accordion size active">
						<h4 class="filter-accordion__title">
							<span>Talle</span>
							<svg class="more"><use xlink:href="#svg-icon-more"></use></svg>
							<svg class="less"><use xlink:href="#svg-icon-less"></use></svg>
						</h4>
						<ul class="filter-accordion__list">
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">35</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">36</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">37</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">38</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">39</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">40</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">41</a>
							</li>
						</ul>
					</div>
					<div class="filter-accordion color">
						<h4 class="filter-accordion__title">
							<span>Color</span>
							<svg class="more"><use xlink:href="#svg-icon-more"></use></svg>
							<svg class="less"><use xlink:href="#svg-icon-less"></use></svg>
						</h4>
						<ul class="filter-accordion__list">
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Amarillo</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Blanco</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Hueso</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Marrón</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Negro</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Nude</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Plata</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Rojo</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Suela</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Vision</a>
							</li>
						</ul>
					</div>
					<div class="filter-accordion price">
						<h4 class="filter-accordion__title">
							<span>Precio</span>
							<svg class="more"><use xlink:href="#svg-icon-more"></use></svg>
							<svg class="less"><use xlink:href="#svg-icon-less"></use></svg>
						</h4>
						<ul class="filter-accordion__list">
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">$2.000 - $3.000</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">$3.000 - $4.000</a>
							</li>
						</ul>
					</div>
					<div class="filter-accordion season">
						<h4 class="filter-accordion__title">
							<span>Temporada</span>
							<svg class="more"><use xlink:href="#svg-icon-more"></use></svg>
							<svg class="less"><use xlink:href="#svg-icon-less"></use></svg>
						</h4>
						<ul class="filter-accordion__list">
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Primavera</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Verano</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Otoño</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Invierno</a>
							</li>
						</ul>
					</div>
					<div class="filter-accordion material">
						<h4 class="filter-accordion__title">
							<span>Material</span>
							<svg class="more"><use xlink:href="#svg-icon-more"></use></svg>
							<svg class="less"><use xlink:href="#svg-icon-less"></use></svg>
						</h4>
						<ul class="filter-accordion__list">
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Algodón</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Piel</a>
							</li>
							<li class="filter-accordion__item">
								<a href="#" class="filter-accordion__item-link">Poliéster</a>
							</li>
						</ul>
					</div>
				</div>
				<a href="#" class="btn-outline primary big plp-filters__apply hidden-lg-up">
					Aplicar
				</a>
			</aside>
			<div class="plp__wrapper">
				<div class="plp__header">
					<div class="plp__total-items">12 Productos</div>
					<div class="plp-filters__trigger hidden-lg-up" id="plp-filter__trigger">FILTRÁ POR <span>+</span></div>
					<div class="plp-sort__wrapper">
						<div class="plp-sort__title">
							<span>ORDENÁ POR<span class="hidden-md-down">:</span></span>
							<span id="show-sort-options">
								<span class="plp-sort__item-active hidden-md-down" id="plp-sort__item-active">Relevancia</span>
								<svg class="arrow-right-full hidden-lg-up"><use xlink:href="#svg-icon-arrow-right-full"></use></svg>
								<svg class="arrow-down hidden-md-down"><use xlink:href="#svg-icon-arrow-down"></use></svg>
							</span>
						</div>
						<div class="plp-sort__content">
							<ul class="plp-sort__list">
								<li class="plp-sort__item active">
									<a href="#" class="plp-sort__item-link">Relevancia</a>
								</li>
								<li class="plp-sort__item">
									<a href="#" class="plp-sort__item-link">A - Z</a>
								</li>
								<li class="plp-sort__item">
									<a href="#" class="plp-sort__item-link">Z - A</a>
								</li>
								<li class="plp-sort__item">
									<a href="#" class="plp-sort__item-link">Menor precio</a>
								</li>
								<li class="plp-sort__item">
									<a href="#" class="plp-sort__item-link">Mayor precio</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="plp-items__wrapper">
					<ul class="plp-items__list" id="plp-items__list">
						<?php
						foreach($items as $item){
						$priceClass = '';
						if($item['listPrice'] != $item['bestPrice']){
							$priceClass = 'promo';
						}
						$listPrice = '$' . money_format('%!i', floatval( $item['listPrice'] ) );
						$bestPrice = '$' . money_format('%!i', floatval( $item['bestPrice'] ) );
						?>
						<li class="product-item__wrapper" id="product-item__wrapper-<?=$item['id']?>">
							<div class="product-item">
								<a href="#" class="product-item__image">
									<picture>
										<source media="(max-width: 767px)" srcset="<?=$item['imageSrcMobile']?>">
										<img src="<?=$item['imageSrc']?>" width="289" height="419" alt="Producto 1">
									</picture>
									<div class="product-item__flags">
									</div>
								</a>
								<h3 class="product-item__name"><?=$item['name']?></h3>
								<?php ?>
								<div class="product-item__price <?=$priceClass?>">
									<div class="product-item__list-price"><?=$listPrice?></div>
									<div class="product-item__sell-price"><?=$bestPrice?></div>
								</div>
								<div class="product-item__installments">Hasta <strong><?=$item['fees']?> cuotas</strong></div>
							</div>
						</li>
						<?php } ?>
					</ul>
					<div class="plp__load-more">
						<a id="load-more-products" class="btn-flat primary" href="#">Ver más productos</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<!-- Footer -->
<?php include("$abs_path/includes/$lang/footer.php");?>
<!-- / End Footer -->

<!-- SVG Sprite -->
<?php include("$abs_path/includes/svg-sprite.php");?>
<!-- / End SVG Sprite -->
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/general-scripts.min.js"></script>
<script type="text/javascript" src="js/plp.min.js"></script>
</body>
</html>